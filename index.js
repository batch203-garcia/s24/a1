const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}.`);

const address = [258, "Washington Ave Nw", "Callifornia", 90011];

const [houseNumber, province, country, zipCode] = address;
console.log(`I live at ${houseNumber} ${province}, ${country} ${zipCode}`);



const animal = {
    name: "Lolong",
    type: "Saltwater Corocodile",
    weight: "1075",
    measurement: "20 ft 3 in"
}

function getAnimalInformation({ name, type, weight, measurement }) {
    console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);
}

getAnimalInformation(animal);

const arrNumbers = [1, 2, 3, 4, 5];

arrNumbers.forEach((numbers) => {
    console.log(`${numbers}`);
})

let reducedNumbers = arrNumbers.reduce((x, y) => x + y, 0);
console.log(reducedNumbers);

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;

    }
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);